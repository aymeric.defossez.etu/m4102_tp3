#Pizza

## Développement d'une ressource *Commande*

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *commande*. Celle-ci devrait répondre aux URI suivantes :

| Opération | URI                    | Action réalisée                               | Retour                                        |
|:----------|:-----------------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /commandes             | récupère l'ensemble des commandes             | 200 et un tableau de commandes                |
| GET       | /commandes/{id}        | récupère la commande d'identifiant id         | 200 et la commande                            |
|           |                        |                                               | 404 si id est inconnu                         |
| GET       | /commandes/{id}/pizzas | récupère les pizzas de la commande            | 200 et un tableau comportant les pizzas de la commande |
|           |                        | d'identifiant id                              | 404 si id est inconnu                         |
| POST      | /commandes             | création d'une commande                       | 201 et l'URI de la ressource créée + représentation |
|           |                        |                                               | 400 si les informations ne sont pas correctes |
|           |                        |                                               | 409 si la commande existe déjà (même nom)     | 
| DELETE    | /commandes/{id}        | destruction de la commande d'identifiant id   | 204 si l'opération a réussi                   |
|           |                        |                                               | 404 si id est inconnu                         |


Une commande comporte un identifiant et une liste de pizzas (ids des pizzas). Sa représentation JSON prendra donc la forme suivante :

{
	"id": 1,
	"pizzas": [1, 5]
}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni par la base de données.

{ "id": 1, "pizzas": [1, 5] }