package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Pizza;

import java.util.ArrayList;
import java.util.List;

public class CommandeCreateDto {

    private String name;
    private List<Pizza> pizzas;

    public CommandeCreateDto() { this.pizzas = new ArrayList<>(); }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Pizza> getPizzas() { return this.pizzas; }

}
