package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import org.glassfish.jersey.server.ResourceConfig;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.ApplicationPath;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
    private static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

    public ApiV1() {
        packages("fr.ulille.iut.pizzaland");

        String environment = System.getenv("PIZZAENV");

        if ( environment != null && environment.equals("withdb") ) {
            LOGGER.info("Loading with database");
            Jsonb jsonb = JsonbBuilder.create();
            try {
                //ingredients
                FileReader reader = new FileReader( getClass().getClassLoader().getResource("ingredients.json").getFile() );
                List<Ingredient> ingredients = JsonbBuilder.create().fromJson(reader, new ArrayList<Ingredient>(){}.getClass().getGenericSuperclass());

                IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
                ingredientDao.dropTable();
                ingredientDao.createTable();
                for ( Ingredient ingredient: ingredients) {
                    ingredientDao.insert(ingredient.getName());
                }

                //pizzas
                reader = new FileReader( getClass().getClassLoader().getResource("pizzas.json").getFile() );
                List<Pizza> pizzas = JsonbBuilder.create().fromJson(reader, new ArrayList<Pizza>(){}.getClass().getGenericSuperclass());

                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
                pizzaDao.dropTableAndIngredientAssociation();
                pizzaDao.createTableAndIngredientAssociation();
                for (Pizza pizza: pizzas) {
                    long id = pizzaDao.insert(pizza.getName());
                    for (Ingredient i : pizza.getIngredients()) {
                        System.out.println("\n\n\n" + i.getName() + "\t" + i.getId() + "\n\n\n");
                        pizzaDao.insert(id, i.getId());
                    }
                }

                //commandes
                reader = new FileReader( getClass().getClassLoader().getResource("commandes.json").getFile() );
                List<Commande> commandes = JsonbBuilder.create().fromJson(reader, new ArrayList<Commande>(){}.getClass().getGenericSuperclass());

                CommandeDao commandeDao = BDDFactory.buildDao(CommandeDao.class);
                commandeDao.dropTableAndPizzaAssociation();
                commandeDao.createTableAndPizzaAssociation();
                for (Commande c: commandes) {
                    long id = commandeDao.insert(c.getName());
                    for (Pizza p : c.getPizzas()) {
                        commandeDao.insert(id, p.getId());
                    }
                }
            } catch ( Exception ex ) {
                throw new IllegalStateException(ex);
            }
        }

    }
}
