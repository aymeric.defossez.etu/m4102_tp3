package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

import java.util.ArrayList;
import java.util.List;

public class Commande {

    private long id;
    private String name;
    private List<Pizza> pizzas;

    public Commande() {
    }

    public Commande(long id, String name) {
        this.id = id;
        this.name = name;
        this.pizzas = new ArrayList<>();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public static CommandeDto toDto(Commande i) {
        CommandeDto dto = new CommandeDto();
        dto.setId(i.getId());
        dto.setName(i.getName());
        dto.setPizzas(i.getPizzas());

        return dto;
    }

    public static Commande fromDto(CommandeDto dto) {
        Commande commande = new Commande();
        commande.setId(dto.getId());
        commande.setName(dto.getName());
        commande.setPizzas(dto.getPizzas());

        return commande;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Commande other = (Commande) obj;
        if (id != other.id)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Commande [id=" + id + ", name=" + name + "]";
    }

    public static CommandeCreateDto toCreateDto(Commande commande) {
        CommandeCreateDto dto = new CommandeCreateDto();
        dto.setName(commande.getName());

        return dto;
    }

    public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
        Commande commande = new Commande();
        commande.setName(dto.getName());

        return commande;
    }

}
